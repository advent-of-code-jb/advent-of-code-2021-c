cmake_minimum_required(VERSION 3.21)
project("Advent of Code 2021 - C++")

set(CMAKE_CXX_STANDARD 20)

set(SOURCE_FILES main.cpp)

add_executable(Aoc_run main.cpp)

include_directories(Aoc_lib)
add_subdirectory(Aoc_lib)

target_link_libraries(Aoc_run Aoc_lib)

add_subdirectory(Google_tests)

