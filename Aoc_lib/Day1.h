//
// Created by jamie on 02/12/2021.
//

#ifndef ADVENT_OF_CODE_2021_C_DAY1_H
#define ADVENT_OF_CODE_2021_C_DAY1_H

#include <string>

namespace aoc {
  class Day1 {
  public:
    static std::string partOne(int *input);
    static std::string partTwo(int *input);
  };
}


#endif//ADVENT_OF_CODE_2021_C_DAY1_H
