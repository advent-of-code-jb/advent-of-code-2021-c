//
// Created by jamie on 02/12/2021.
//

#include "Day1.h"
#include "Helper.h"

using namespace std;
using namespace aoc;
using namespace aoc_helper;

string Day1::partOne(int *input) {

  int numberOfIncrements = 0;
  int previous;
  for(int i = 0; i < Helper::length(input); i++){
    if(previous){
      if(input[i] > previous){
        numberOfIncrements++;
      }
    }
    previous = input[i];
  }

  return to_string(numberOfIncrements);
}


string Day1::partTwo(int *input) {
  
  return string();
}
