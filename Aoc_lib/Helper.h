//
// Created by jamie on 03/12/2021.
//

#ifndef ADVENT_OF_CODE_2021_C_HELPER_H
#define ADVENT_OF_CODE_2021_C_HELPER_H

namespace aoc_helper {


  class Helper {
  public:
    static int length(auto *array);
  };

}// namespace aoc_helper

#endif//ADVENT_OF_CODE_2021_C_HELPER_H
