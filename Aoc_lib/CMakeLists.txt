project(Aoc_lib)

set(HEADER_FILES
        Helper.h
        Day1.h
        )

set(SOURCE_FILES
        Helper.cpp
        Day1.cpp
        )

add_library(Aoc_lib STATIC ${SOURCE_FILES} ${HEADER_FILES})