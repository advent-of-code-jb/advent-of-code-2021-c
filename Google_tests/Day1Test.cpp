//
// Created by jamie on 02/12/2021.
//

#include "Day1.h"
#include "gtest/gtest.h"

using namespace aoc;

class Day1Fixture : public ::testing::Test {
protected:
  virtual void SetUp() {
    solution = new Day1();
  }

  virtual void TearDown(){
    delete solution;
  }

  Day1 *solution;
};

TEST_F(Day1Fixture, partOne_shouldReturn0WhenNothingEntered){
  // Given
  int input[0] = {};
  // When
  std::string result = aoc::Day1::partOne(input);
  // Then
  EXPECT_EQ(result,"0");
}

TEST_F(Day1Fixture, partOne_shouldReturn0AsAnswerWhen1ValueEntered){
  // Given
  int input[1] = {123};
  // When
  std::string result = aoc::Day1::partOne(input);
  // Then
  EXPECT_EQ(result,"0");
}

TEST_F(Day1Fixture, partOne_shouldReturn0AsAnswerWhenNoIncreasingValues){
  // Given
  int input[3] = {1000, 100, 10};
  // When
  std::string result = aoc::Day1::partOne(input);
  // Then
  EXPECT_EQ(result,"0");
}

TEST_F(Day1Fixture, partOne_shouldReturn0AsAnswerWhenDuplicateValues){
  // Given
  int input[3] = {1000, 1000};
  // When
  std::string result = aoc::Day1::partOne(input);
  // Then
  EXPECT_EQ(result,"0");
}

TEST_F(Day1Fixture, partOne_shouldReturn1AsAnswerWhen1IncreasingValue){
  // Given
  int input[3] = {1000, 1001};
  // When
  std::string result = aoc::Day1::partOne(input);
  // Then
  EXPECT_EQ(result,"1");
}